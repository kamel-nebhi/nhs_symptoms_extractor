from bs4 import BeautifulSoup as BS
import requests
import re
from collections import OrderedDict
from nltk import sent_tokenize, word_tokenize, pos_tag, RegexpParser
from nltk.tree import Tree


class SymptomExtractor(object):

    def __init__(self):
        pass

    def ie_preprocess(document):
        "returns symptoms chunks in a given text"
        sentences = sent_tokenize(document)
        tokenized = [word_tokenize(sentence) for sentence in sentences]
        pos_tags = [pos_tag(sentence) for sentence in tokenized]

        patterns = """
                SYMPTOM:
                {^<NN><NN>}
                {<NN. *><IN><NN. *>}
                {^<JJ>$}
                {^<NN>}
                {<VBG><RB><JJ>|<VBG><RB>|<RB><JJ>}
                {<JJ><NN><NN>|<JJ><NNS>}
                """

        SChunker = RegexpParser(patterns)
        chunked_sents = [SChunker.parse(sentence) for sentence in pos_tags]

        return chunked_sents

    def find_symptoms_kw(chunks):
        "given list of tagged parts of speech, returns unique named entities"

        def traverse(tree):
            "recursively traverses an nltk.tree.Tree to find named entities"

            symptom_kw = []

            if hasattr(tree, 'label') and tree.label:
                if tree.label() == 'SYMPTOM':
                    symptom_kw.append(' '.join([child[0] for child in tree]))
                else:
                    for child in tree:
                        symptom_kw.extend(traverse(child))

            return symptom_kw

        symptom_kw_entities = []

        for chunk in chunks:

            entities = sorted(list(set([word for tree in chunk
                                        for word in traverse(tree)])))
            for e in entities:
                if e not in symptom_kw_entities:
                    symptom_kw_entities.append(e)
        return symptom_kw_entities

    def list_symptoms(self, url):
        page = requests.get(url)

        soup = BS(page.content,'lxml')

        symptoms = {}
        symptomsCandidate = []
        num = 0

        for p in soup.find_all("p", text=re.compile('symptoms of (.+) include:$|the following symptoms:$|symptoms such as:$|you may:$|see your GP if:$|include:$', re.IGNORECASE)):
            symptomLiList = p.find_next('ul').find_all('li')


            for li in symptomLiList:
                symptoms_raw = {}
                symptomLi = li.text
                symptoms_raw["symptom_raw"] = symptomLi
                entity_chunks = SymptomExtractor.ie_preprocess(symptomLi)
                symptom_kw_entities = SymptomExtractor.find_symptoms_kw(entity_chunks)
                if symptom_kw_entities:
                    symptoms_raw["symptom_kws"] = symptom_kw_entities
                num +=1
                symptomsCandidate.append(symptoms_raw)

        symptoms["number_of_symptoms"] = num
        symptoms["symptoms"] = symptomsCandidate

        key_order_dict = ['number_of_symptoms', 'symptoms']
        data = OrderedDict(sorted(symptoms.items(), key=lambda i: key_order_dict.index(i[0])))

        return data
