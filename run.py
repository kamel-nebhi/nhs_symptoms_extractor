from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
from nhs import SymptomExtractor

app = Flask(__name__)
api = Api(app)


class NhsExtractorApi(Resource):
    def get(self):
        extractor = SymptomExtractor()

        parser = reqparse.RequestParser()
        parser.add_argument('url', type=str, case_sensitive=False, trim=True, required=True, help='No url provided')

        args = parser.parse_args()
        url = args.get('url')

        if url:
            result = extractor.list_symptoms(url)

        return jsonify({"result": result})


api.add_resource(NhsExtractorApi, '/nhs_symptoms_extractor')

if __name__ == '__main__':
    app.run(debug=True)