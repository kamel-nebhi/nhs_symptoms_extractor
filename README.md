# How to use the NHS symptoms extractor

This application allows the extraction of symptoms on NHS webpages. 

## Approach
The approach to extract symptoms is mainly based on HTMLparsing and RegExp to extract patterns related to symptoms
for a specific disease.

Foreach raw symptom extracted in the HTML structure, we apply grammar rules using NLTK toolkit.

This application exploit the logical structure of the NHS webpages to extract symptoms and provides symptoms keyword using grammar rules based on PoS tagging and chunking.

To create the extractor we used this files:
* http://www.nhs.uk/Conditions/Heart-block/Pages/Symptoms.aspx
* http://www.nhs.uk/conditions/coronary-heart-disease/Pages/Symptoms.aspx
* http://www.nhs.uk/Conditions/Depression/Pages/Symptoms.aspx
* http://www.nhs.uk/conditions/Bronchitis/Pages/Introduction.aspx
* http://www.nhs.uk/conditions/warts/Pages/Introduction.aspx

## Run the Flask Web application

Install the requirements:
```sh
pip3 install requirements
```

Run the Flask App:
```sh
python3 run.py
```

Try the endpoint with curl:
```sh
curl http://127.0.0.1:5000/nhs_symptoms_extractor?url=<URL>
```
For example:
```
curl http://127.0.0.1:5000/nhs_symptoms_extractor?url=http://www.nhs.uk/Conditions/Heart-block/Pages/Symptoms.aspx
```


For Heart-block the results should be a json file:
```json
{
  "result": {
    "number_of_symptoms": 18, 
    "symptoms": [
      {
        "symptom_kws": [
          "light-headedness"
        ], 
        "symptom_raw": "light-headedness "
      }, 
      {
        "symptom_kws": [
          "dizziness"
        ], 
        "symptom_raw": "dizziness "
      }, 
      {
        "symptom_kws": [
          "fainting", 
          "loss of consciousness"
        ], 
        "symptom_raw": "fainting\u00a0(temporary loss of consciousness) "
      }, 
      {
        "symptom_kws": [
          "chest pain"
        ], 
        "symptom_raw": "chest pain, which may be worse during physical activity, such as climbing the stairs "
      }, 
      {
        "symptom_kws": [
          "shortness of breath"
        ], 
        "symptom_raw": "shortness of breath\u00a0 "
      }, 
      {
        "symptom_kws": [
          "tiring easily"
        ], 
        "symptom_raw": "tiring easily when doing physical activity "
      }, 
      {
        "symptom_kws": [
          "feeling very", 
          "low blood pressure"
        ], 
        "symptom_raw": "feeling very dizzy suddenly when standing up from a lying or sitting position; this is caused by having\u00a0low blood pressure (hypotension) "
      }, 
      {
        "symptom_kws": [
          "unusually pale"
        ], 
        "symptom_raw": "unusually pale and blotchy skin "
      }, 
      {
        "symptom_kws": [
          "lack of energy", 
          "lethargy"
        ], 
        "symptom_raw": "lethargy (a lack of energy) "
      }, 
      {
        "symptom_kws": [
          "part in exercise"
        ], 
        "symptom_raw": "unwillingness to take part in exercise or physical activity "
      }, 
      {
        "symptom_kws": [
          "dizziness"
        ], 
        "symptom_raw": "dizziness "
      }, 
      {
        "symptom_kws": [
          "fainting"
        ], 
        "symptom_raw": "fainting "
      }, 
      {
        "symptom_kws": [
          "light-headedness"
        ], 
        "symptom_raw": "light-headedness "
      }, 
      {
        "symptom_kws": [
          "dizziness"
        ], 
        "symptom_raw": "dizziness "
      }, 
      {
        "symptom_kws": [
          "fainting"
        ], 
        "symptom_raw": "fainting "
      }, 
      {
        "symptom_kws": [
          "fatigue"
        ], 
        "symptom_raw": "fatigue (extreme tiredness) "
      }, 
      {
        "symptom_kws": [
          "chest pain"
        ], 
        "symptom_raw": "chest pain "
      }, 
      {
        "symptom_kws": [
          "slow heart beat"
        ], 
        "symptom_raw": "slow heart beat (bradycardia) "
      }
    ]
  }
}
```
## Evaluation

For the evaluation of the application, we use these documents:
* http://www.nhs.uk/conditions/Sleep-paralysis/Pages/Introduction.aspx
* http://www.nhs.uk/Conditions/Glue-ear/Pages/Symptoms.aspx
* http://www.nhs.uk/Conditions/Turners-syndrome/Pages/Symptoms.aspx
* http://www.nhs.uk/Conditions/Obsessive-compulsive-disorder/Pages/Symptoms.aspx
* http://www.nhs.uk/conditions/frozen-shoulder/Pages/Symptoms.aspx

We actually use the raw symptoms to evaluate the system. For now, the accuracy achieved around 55%.

## Possible improvement
To improve the current prototype, we could:
* improve grammatical rules for keyword extraction
* improve current RegExp-HTMLparsing to improve the results (currently it is mainly based on ul tags)
* exploit wikipedia pages to extract bag of words for each disease
* add dictionary to improve the accuracy


